import { UrlPublic } from './../urlpublic';
import { PackagePage } from './../package/package';
import { Component } from '@angular/core';
import { AlertController, NavController, LoadingController, ToastController, NavParams } from 'ionic-angular';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Http, Headers } from '@angular/http';
import { CompleteTestService } from './../auto';

@Component({
  selector: 'page-ads',
  templateUrl: 'ads.html'
})
export class AdsPage {

  token: any;
  userId: any;
  adsId: any;
  title: any;
  description: any;
  cities : any;
  marketerTypeId: any;
  marketerType: any;
  // imageURI:any;
  // imageFileName:any;
  userAdsImg: any;
  typeDisable : boolean;
  createable: boolean;
  editable: boolean;
  bigImg = null;
  // bigSize = '0';
  // public myPhoto: any; 
  file: any;
  folderName: any = 'ads'; 

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public urlPublic: UrlPublic,
    public sanitizer: DomSanitizer,
    public http: Http,
    private transfer: FileTransfer,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public completeTestService: CompleteTestService
    ) {
      this.token = navParams.get('token');
      this.userId = navParams.get('userId');
      this.adsId = navParams.get('adsId');
      this.getMarket()
      if(this.adsId != undefined) {
        this.getAds(this.adsId)
        this.typeDisable = true;
        this.createable = false;
        this.editable = true;
      } else {
        this.typeDisable = false;
        this.createable = true;
        this.editable = false;
      }
  }

  // get ads if editable

  getAds(adsId) {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Adds/Vendor/' + adsId, {headers: header}).subscribe((ads) => {
      let allAds = ads.json();
      console.log("test",allAds)
      // let findAds = allAds.find(x => x.adsId == adsId);
      this.title = allAds.title;
      this.bigImg = allAds.imgUrl;
      this.description = allAds.description;
      this.marketerTypeId = allAds.cityId;
      this.http.get(this.urlPublic.apiUrl + 'api/Users/City/' + this.marketerTypeId).subscribe(city => {
        let findcity = city.json();
        this.marketerType = findcity.nama;
      })
      var header1 = new Headers();
        header1.append('Authorization', 'bearer' + ' ' + this.token)
        this.http.get(this.urlPublic.apiUrl + 'api/Image?imagePath=' + this.bigImg, {headers: header1}).subscribe(imgData => {
          let url = imgData.url
          this.userAdsImg = url.replace("'\'", "'\\'")
          console.log(this.userAdsImg)
          this.getImgContent();
        })
    })
  }

  getImgContent(): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(this.userAdsImg);
  }

  // get ads if editable

  // marketer type

  getMarket() {
    this.http.get(this.urlPublic.apiUrl + 'api/Users/City').subscribe((data) => {
      this.cities = data.json()
    })
  }

  // marketer type

  // upload image

  changeListener(event) {
    
    this.file = event.target.files[0];
    console.log("get pic", this.file)
  }

  

  // upload image

  // submit ads

  submitAds() {
    var fd = new FormData();
    fd.append('file', this.file, this.file.name)
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    header.append('ResponseType', 'text' as 'text')

    this.http.post(this.urlPublic.apiUrl + 'api/Image?folderPath=' + this.folderName, fd, {headers: header}).subscribe(
      data => {
        
        let dataImage = data.text();
        console.log("image transfer", dataImage)
        let adsJson = {
          title: this.title,
          imgUrl: dataImage,
          description: this.description,
          cityId: this.marketerType,
          userId: this.userId
        }
        var header = new Headers();
        header.append('Authorization', 'bearer' + ' ' + this.token);
        header.append('Content-Type', 'application/json');
        this.http.post(this.urlPublic.apiUrl + 'api/Adds/Vendor', adsJson, {headers: header}).subscribe(
            data => {
              let dataAds = data.json();
              console.log(dataAds)
              this.presentPrompt("Konfirmasi Berhasil", "Iklan Anda telah terdaftar")
            }
          ), err => {
            this.presentLoading("failed")
          }
      }
    )
  }
  

  // submit ads


  // edit ads

  editAds() {
    // this.presentLoading("")
    // const fileTransfer: FileTransferObject = this.transfer.create();
    // var header1 = new Headers();
    // header1.append('Authorization', 'bearer' + ' ' + this.token);
    // header1.append('Content-Type', 'multipart/form-data');
  
    // let options: FileUploadOptions = {
    //   chunkedMode: false,
    //   fileKey: "imgUrl",
    //   httpMethod: "POST",
    //   mimeType: "image/jpeg",
    //   headers: header1
    // }
  
    // fileTransfer.upload(this.imageURI, this.urlPublic.apiUrl + 'api/Adds/Vendor', options)
    //   .then((data) => {
    //   console.log(data+" Uploaded Successfully");
    //   let adsJson = {
    //     title: this.title,
    //     imgUrl: data,
    //     description: this.description,
    //     cityId: this.marketerType
    //   }
    //   var header = new Headers();
    //   header.append('Authorization', 'bearer' + ' ' + this.token);
    //   header.append('Content-Type', 'application/json');

    //   this.http.post(this.urlPublic.apiUrl + 'api/Adds/Vendor', adsJson, {headers: header}).subscribe(
    //     data => {
    //       let dataAds = data.json();
    //       console.log(dataAds)
    //       this.presentPrompt("Konfirmasi Berhasil", "Iklan Anda telah terdaftar")
    //     }
    //   )
    // }, (err) => {
    //   console.log(err);
    // });
  }

  // edit ads


  // alert

  presentPrompt(titlePrompt, messagePrompt) {
    let nextStep = this.alertCtrl.create({
      title: titlePrompt,
      message: messagePrompt,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            if(titlePrompt == "Konfirmasi Berhasil"){
              window.location.reload()
            }
          }
        }
      ]
    })
    nextStep.present()
  }

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // alert

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

  

}
