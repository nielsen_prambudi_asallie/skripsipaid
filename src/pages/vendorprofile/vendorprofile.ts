import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { UrlPublic } from './../urlpublic';
import { PackagePage } from './../package/package';
import { UpdateBiodataPage } from './../updatebiodata/updatebio';
import { UploadTransferPage } from './uploadtrf/uploadtrf';
import { AdsPage } from './../ads/ads';
import { Component } from '@angular/core';
import { AlertController, NavController, LoadingController, ToastController, NavParams } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Http, Headers } from '@angular/http';

// ctrl+F untuk mencari function di bawah ini :

// get user profile
// get user ads
// get user point
// get pending point
// ads detil
// ads delete
// update Biodata
// add ads
// add poin
// log out
// alert
// loading


@Component({
  selector: 'page-vendorprofile',
  templateUrl: 'vendorprofile.html'
})
export class VendorProfilePage {

  token: any;
  userId: any;
  cityId: any;
  name: string;
  address: string;
  city: string;
  phone: any;
  pointAmount: any;
  pending: any;
  userAds: any;
  userAdsImg: any;
  public myPhoto: any;
  bigImg = null;
  bigSize = '0'; 
  imageURI:any;
  imageFileName:any;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public urlPublic: UrlPublic,
    private transfer: FileTransfer,
    public http: Http,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public sanitizer: DomSanitizer) {
    this.token = navParams.data.token
    this.userId = navParams.data.userId
    console.log("profile token", this.token)
    this.getUserProfile()
    this.getUserPoint()
    this.getUserAds()
    this.getPendingPoin()
    this.pointAmount = 0
  }

  // get user profile

  getUserProfile() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Users/' + this.userId, {headers: header}).subscribe(data => {
      let userBio = data.json();
      this.name = userBio.name;
      this.address = userBio.address;
      this.phone = userBio.phone;
      this.cityId = userBio.cityId;
      console.log(this.cityId)
      this.http.get(this.urlPublic.apiUrl + 'api/Users/City/' + this.cityId).subscribe(city => {
        let allCity = city.json();
        this.city = allCity.nama;
      })
    })
    
    
  }

  // get user profile



  // get user ads

  getUserAds() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    // this.http.get(this.urlPublic.apiUrl + 'api/Adds/User/' + this.userId, {headers: header}).subscribe(adsData => {
    this.http.get(this.urlPublic.apiUrl + 'api/Adds/UserExtend/' + this.userId, {headers: header}).subscribe(adsData => {
      this.userAds = adsData.json();
      console.log("user ads", this.userAds);
    })

  }

  // get user ads




   // get user point

   getUserPoint() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Poin/Sum/' + this.userId, {headers: header}).subscribe(pointData => {
      let userPoint = pointData.json();
      this.pointAmount = userPoint
      
    })
    
    
  }

  // get user point


  // get pending point

  getPendingPoin() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    this.http.get(this.urlPublic.apiUrl + 'api/Poin/Purchase/User/' + this.userId, {headers: header}).subscribe((pend) => {
      this.pending = pend.json();
    })
  }

  // get pending point
  

  // ads detil

  adsDetil(adsId) {
    this.navCtrl.push(AdsPage, {
      adsId: adsId,
      userId: this.userId,
      token: this.token
    })
  }

  // ads detil


  // update Biodata

  updateBio() {
    this.navCtrl.push(UpdateBiodataPage, {userId : this.userId, token : this.token})
  }

  // update Biodata

  goToUpload(poinId){
    this.navCtrl.push(UploadTransferPage, {userId: this.userId, token : this.token, poin: poinId})
  }

  // add ads

  backToAds() {
    this.navCtrl.push(AdsPage, {userId : this.userId, token : this.token})
  }

  // add ads


  // add poin

  backToPackage() {
    this.navCtrl.push(PackagePage, {userId : this.userId, token : this.token})
  }

  // add poin

  // log out

  logOut() {
    window.localStorage.removeItem('username');
    window.localStorage.removeItem('password');
    this.presentLoading('');
    window.location.reload();
  }

  // log out

  // alert

  presentPrompt(titlePrompt, messagePrompt) {
    let nextStep = this.alertCtrl.create({
      title: titlePrompt,
      message: messagePrompt,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            if(titlePrompt == "Konfirmasi Berhasil"){
              window.location.reload()
            }
          }
        }
      ]
    })
    nextStep.present()
  }

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // alert

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

}
