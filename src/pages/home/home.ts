
import { UrlPublic } from './../urlpublic';
import { AdminPage } from './../admin/admin';
import { AdminReportPage } from './../adminreport/adminreport';
import { VendorProfilePage } from './../vendorprofile/vendorprofile';
import { PackagePage } from './../package/package';
import { AdsPage } from './../ads/ads';
import { MarketerAdsPage } from './../marketerads/marketerads';
import { MarketerProfilePage } from './../marketerprofile/marketerprofile';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';

// ctrl+F untuk mencari function di bawah ini :

// roots to any page
// get type register

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // roots to any page
  ads = AdsPage;
  marketerAds = MarketerAdsPage;
  package = PackagePage;
  profile = VendorProfilePage;
  marketerProfile = MarketerProfilePage;
  adminPage = AdminPage;
  adminReportPage = AdminReportPage;
  // roots to any page

  regTypeId: any;
  regType: any;

  user = {
    token: '',
    userId: ''
  }
  

  constructor(public navCtrl: NavController,
     public urlPublic: UrlPublic,
     public navParams: NavParams,
     public http: Http) {
    let type = navParams.get('type');
    let token = navParams.get('token');
    let userId = navParams.get('userId');
    this.regTypeId = type;
    this.user.token = token;
    this.user.userId = userId;
    // let getItemPassAdmin = window.localStorage.getItem("password");
    // let getItemUsrAdmin = window.localStorage.getItem("username");
    // if (getItemPassAdmin === "admin" && getItemUsrAdmin === "admin") {
    //   this.regType = "Admin";
    // } else {
      this.getTypeName(type);
    // };
    // console.log("get item", window.localStorage.getItem("password"));
  }

  // get type register
  getTypeName(type) {
    if (type === 1) {
      this.regType = "Vendor"
    }
    if (type === 2) {
      this.regType = "Marketer"
    } 
    if (type === 3) {
      this.regType = "Admin"
    }
    // this.http.get(this.urlPublic.apiUrl + 'api/Users/RegisterType').subscribe((reg) => {
    //   let type = reg.json()
    //   let findType = type.find(x => x.regTypeId == this.regTypeId)
    //   this.regType = findType.regType1
    // })

  }
  // get type register


  

  

}
