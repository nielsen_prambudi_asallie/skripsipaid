import { UrlPublic } from './../urlpublic';
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Http, Headers } from '@angular/http';
import {AutoCompleteService} from 'ionic2-auto-complete';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map'
import { CompleteTestService } from './../auto';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})

// @Injectable()
// export class CompleteTestService implements AutoCompleteService {
//   labelAttribute = "name";
//   formValueAttribute = ""

//   constructor(private http:Http) {
    
//   }
//   getResults(keyword:string) {
//     return this.http.get("this.urlPublic.apiUrl + 'api/Users/City?search='"+keyword)
//       .map(
//         result =>
//         {
//           return result.json()
//             .filter(item => item.name.toLowerCase().startsWith(keyword.toLowerCase()) )
//         });
//   }
// }
export class RegisterPage {

  

  name      : '';
  email     : '';
  address   : '';
  city      : '';
  phone     : '';
  regType   : '';
  username  : '';
  password  : '';
  cities : any;
  register : any;
  testing : any;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public urlPublic: UrlPublic,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public http: Http,
    public completeTestService: CompleteTestService
    ) {
      this.getCity()
      this.getRegType();
      let usrname = navParams.get('username');
      let email = navParams.get('email');
      let password = navParams.get('password')
      console.log("password from FB", password)
      if (usrname != '' && email != '' && password != '') {
        this.username = usrname;
        this.email = email;
        this.password = password;
      }
      
  }


  // get city
  getCity() {
    this.http.get(this.urlPublic.apiUrl + 'api/Users/City').subscribe((data) => {
      this.cities = data.json()
    })

    this.getRegType()
    
  }
  // get city



  // get reg type
  getRegType() {
    this.http.get(this.urlPublic.apiUrl + 'api/Users/RegisterType').subscribe((reg) => {
      this.register = reg.json()
    })
  }
  // get reg type

  

  // submit register
  registerBegin() {
    // let cit = this.city;
    let registerVar = JSON.stringify({
      name: this.name,
      email: this.email,
      address: this.address,
      cityId: this.city,
      phone: this.phone,
      userName: this.username,
      password: this.password,
      regType: this.regType,
    })
    console.log("register variable", registerVar);
    // this.sendVerificationCode(this.phone, '+62');
    var header = new Headers();
    header.append('Content-Type', 'application/json');
    header.append('Access-Control-Allow-Origin', '*');
    this.presentLoading("")
    this.http.post(this.urlPublic.apiUrl + 'api/Users/register', registerVar, {headers: header}).subscribe(
      reg => {
        let reguser = reg.json();
        console.log(reguser);
        this.waitConfirm("Konfirmasi", "Registrasi berhasil");
      }, err => {
        this.presentLoading("failed")
      }
    )

    
  }

  sendVerificationCode(): any {
    // this.presentVerification(phone_number, country_code);
    // let ver = JSON.stringify({
    //   countryCode: country_code,
    //   phoneNumber: phone_number,
    //   via: "SMS"
    // });
    let ver = {
      countryCode: "+62",
      phoneNumber: this.phone,
      via: "SMS"
    };
    var header = new Headers();
    header.append('Content-Type', 'application/json');
    header.append('Access-Control-Allow-Origin', '*');
    this.http.post(this.urlPublic.apiUrl + 'api/Phone/start', ver, {headers: header}).subscribe(
      ver => {
        let verify = ver.json();
        console.log("success sms", verify);
        this.presentVerification(this.phone, "+62");
      }, err => {
        this.errorNumbAlert();
        console.log("error verify", err.message);
      }
    )
}

getCode(verNumber, phoneNumber, countryCode) {
// getCode() {
  // let twilioapiurl = 'https://api.authy.com/protected/json/';
  // let apikey = 'HdVOwxWYXKWzSipZkX1rVliCMbJW2r9n';
  // // let tokentwi = 'c083ce8d3455619adaba03af664820d1';
  // let tokentwi = 'e8b447720aa531dc08231e37651edbd0';
  // let header = new Headers();
  // header.append('Content-type', 'application/json');
  // header.append('Authorization', 'bearer' + ' ' + tokentwi)
  // this.http.get(twilioapiurl + 'phones/verification/check?api_key=' + apikey +
  // '&country_code=' + countryCode + '&phone_number=' + phoneNumber + '&verification_code=' + verNumber, {headers: header}).subscribe(
  //   ver => {
  //     let code = ver.json();
  //     console.log("success verification", code);
  //     this.navCtrl.push(LoginPage);
  //   }, err => {
  //     console.log("failed confirm");
  //   }
  // );
  // let vernumb = {
  //   token: parseInt(verNumber)
  // };
  let vernumb = JSON.stringify({
    token: verNumber,
    CountryCode: countryCode,
    PhoneNumber: phoneNumber
  });
  var header = new Headers();
  header.append('Content-Type', 'application/json');
  header.append('Accept', 'application/json');
  this.http.post(this.urlPublic.apiUrl + 'api/phone/verify', vernumb, {headers: header}).subscribe(
    number => {
      let numb = number.json();
      console.log("succeded", numb.succeeded)
      if(numb.succeeded != false) {
        // this.waitConfirm("Konfirmasi", "Verifikasi sukses");
        this.registerBegin();
      } else {
        this.errorNumbAlert();
      }
      
      console.log("success confirm", numb);
    }, err => {
      this.presentLoading("failed");
      console.log("error confirm", err);
    }
  )
  
}

presentVerification(phoneNumber, country_code) {
  let prompt = this.alertCtrl.create({
      title: 'Verifikasi',
      message: 'Masukkan Kode Verifikasi Anda',
      inputs: [
        {
          name: 'verificationCode',
          placeholder: 'Kode Verifikasi'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log("cancel")
            // this.waitConfirm('Kode Salah', 'Kode yang Anda Masukkan Salah' )
          }
        },
        {
          text: 'Submit',
          handler: data => {
            console.log(data.verificationCode)
            this.getCode(data.verificationCode, phoneNumber, country_code);
            // this.navCtrl.push(LoginPage);
            // this.waitConfirm('Konfirmasi', 'Kode verifikasi telah diterima, silahkan login menggunakan username dan kata sandi terdaftar' )
          }
        }
      ]
    })
    prompt.present()
}

  waitConfirm(titlePrompt, messagePrompt) {
    if (titlePrompt == 'Konfirmasi') {
      let confirm = this.alertCtrl.create({
        title: titlePrompt,
        message: messagePrompt,
        buttons: [
          {
            text: 'Login',
            handler: data => {
              this.navCtrl.push(LoginPage);
            }
          }
        ]
      })
    
      confirm.present()
    }
    else {
      let confirm = this.alertCtrl.create({
        title: titlePrompt,
        message: messagePrompt,
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              this.registerBegin();
            }
          }
        ]
      })
    
      confirm.present()
    }
  }
  // submit register

  // back to login
  login() {
    this.navCtrl.push(LoginPage)
  }
  // back to login

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

  // Alert

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // Alert

  errorNumbAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, cek kembali apakah anda telah memasukkan nomor handphone yang sama atau tidak",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

}
