import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, ViewController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { UrlPublic } from './../../urlpublic';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { SocialSharing } from '@ionic-native/social-sharing';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-modalimg',
  templateUrl: 'modalimg.html'
})
export class ModalImgPage {

  token: any;
  typeModal: any;
  userPoinId: any;
  userId: any;
  image: any;
  points: any;
  title: any;
  description: any;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public socialSharing: SocialSharing,
    public viewCtrl: ViewController,
    public urlPublic: UrlPublic,
    public navParams: NavParams,
    public sanitizer: DomSanitizer,
    public loadingCtrl: LoadingController,
    public http: Http
    ) {
      this.token = navParams.get('token');
      this.userPoinId = navParams.get('userPoinId');
      this.userId = navParams.get('userId');
      this.getImage()
  }


  // get poin for user vendor
  getImage() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    this.http.get(this.urlPublic.apiUrl + 'api/Poin/Purchase/Uploaded/List', {headers: header}).subscribe((pend) => {
      
      let userVendor = pend.json();
      let specificImage = userVendor.filter(x => x.userId == this.userId && x.userPoinId == this.userPoinId)
      let gettingthis = specificImage
      console.log(gettingthis)
      for(var image of gettingthis) {
        console.log(image.userPointImage.imageUrl)
        let imageUrl = image.userPointImage.imageUrl
        this.http.get(this.urlPublic.apiUrl + 'api/Image?imagePath=' + imageUrl, {headers:header}).subscribe(
          img => {
            this.image = img.url
            console.log(this.image)
          }
        ) 
      }

      
    })

  }

  

  dismiss() {
    this.viewCtrl.dismiss()
  }

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

  // Alert

  presentPrompt(titlePrompt, messagePrompt) {
    let nextStep = this.alertCtrl.create({
      title: titlePrompt,
      message: messagePrompt,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            window.location.reload()
          }
        }
      ]
    })
    nextStep.present()
  }

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // Alert
  

  
  

  

}
