import { ModalImgPage } from './../modalimg/modalimg';
import { Component } from '@angular/core';
import { ModalController, NavController, AlertController, LoadingController, ViewController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { UrlPublic } from './../../urlpublic';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { SocialSharing } from '@ionic-native/social-sharing';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-modalver',
  templateUrl: 'modalver.html'
})
export class ModalVerPage {

  token: any;
  typeModal: any;
  userId: any;
  showImg: boolean;
  image: any;
  points: any;
  pointsReq: any;
  title: any;
  description: any;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public socialSharing: SocialSharing,
    public viewCtrl: ViewController,
    public urlPublic: UrlPublic,
    public navParams: NavParams,
    public sanitizer: DomSanitizer,
    public loadingCtrl: LoadingController,
    public http: Http
    ) {
      this.token = navParams.get('token');
      this.userId = navParams.get('userId');
      this.typeModal = navParams.get('typeModal');
      console.log(this.userId)
      this.showImg = false;
      this.getAllPoint()
      this.getAllPointReq()
  }


  // get poin for user vendor
  getAllPoint() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    this.http.get(this.urlPublic.apiUrl + 'api/Poin/Purchase/Uploaded/List', {headers: header}).subscribe((pend) => {
      
      let userVendor = pend.json();
      let specificVendor = userVendor.filter(x => x.userId == this.userId)
      this.points = specificVendor
      // this.pathImg = this.points.userPointImage.imageUrl
      console.log(this.points)

      
    })
  }

  getImage(userPoinId) {
    let modal = this.modalCtrl.create(ModalImgPage, {
      userPoinId : userPoinId,
      userId: this.userId,
      token : this.token
    });
    modal.present();
      }

  
  // get poin for user vendor

  // get poin for user marketer
  getAllPointReq() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    this.http.get(this.urlPublic.apiUrl + 'api/Marketer/Verify/List', {headers: header}).subscribe((pend) => {
      
      let userMarketer = pend.json();
      let specificMarketer = userMarketer.filter(x => x.userId == this.userId)
      this.pointsReq = specificMarketer
      // this.pathImg = this.points.userPointImage.imageUrl
      console.log(this.pointsReq)

      
    })
  }

  
  
  // get poin for user marketer


  verifyPurchase(userPoinId) {
    console.log(this.token)
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    this.presentLoading("")
    this.http.get(this.urlPublic.apiUrl + 'api/poin/purchase/Verify/' + userPoinId, {headers: header}).subscribe(
      (res) => {
        console.log("verify purchase", res.json())
        
      }

    )
    this.presentPrompt("Konfirmasi Berhasil", "verifikasi poin berhasil")
    
  }

  verifyRequest(userPoinId) {
    console.log(userPoinId)
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    this.presentLoading("")
    this.http.get(this.urlPublic.apiUrl + 'api/Marketer/Release/Verify/' + userPoinId, {headers: header}).subscribe(
      (res) => {
        console.log("verify purchase", res.json())
        
      }

    )
    this.presentPrompt("Konfirmasi Berhasil", "verifikasi poin berhasil")
    
  }


  dismiss() {
    this.viewCtrl.dismiss()
  }

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

  // Alert

  presentPrompt(titlePrompt, messagePrompt) {
    let nextStep = this.alertCtrl.create({
      title: titlePrompt,
      message: messagePrompt,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            window.location.reload()
          }
        }
      ]
    })
    nextStep.present()
  }

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // Alert
  

  
  

  

}
