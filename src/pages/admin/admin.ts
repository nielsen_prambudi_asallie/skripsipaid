import { UrlPublic } from './../urlpublic';
import { ModalVerPage } from './modalver/modalver';
import { Component } from '@angular/core';
import { ModalController, NavController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ModalVerSharePage } from './modalvershare/modalvershare';

@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html'
})
export class AdminPage {

  token: any;
  userId: any;
  vendors: any;
  marketers: any;
  points: any; 
  startDateAcc: any;
  endDateAcc: any; 
  accads: any; 

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public urlPublic: UrlPublic,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public http: Http,
    public sanitizer: DomSanitizer
    ) {
      this.token = navParams.data.token;
      this.userId = navParams.data.userId;
      console.log(this.token);
      this.getUserVendor();
      this.getUserMarketer();
      this.getAllPoint();
  }

  doRefresher(refresher){
    this.getUserVendor();
    this.getUserMarketer();
    this.getAllPoint();
    console.log(this.getAllPoint);
    console.log(this.getAllPointReq)

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  getAllPoint() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    this.http.get(this.urlPublic.apiUrl + 'api/Poin/Purchase/Uploaded/List', {headers: header}).subscribe((pend) => {
      
      let userVendor = pend.json();
      let vend = [];
      userVendor.forEach(function (row, i) {
        vend.push({
          user: row.user
        })
      })
      this.vendors = vend;

      // let specificVendor = userVendor.filter(x => x.userId == this.userId);
      // this.points = specificVendor;
      // this.pathImg = this.points.userPointImage.imageUrl
      console.log("get all points", vend);

      
    })
  }

  getAcceptedAds() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Adds/Marketer/Adds/Imageverify?startDate=' + this.startDateAcc + '&endDate=' + this.endDateAcc, {headers: header}).subscribe(allAds => {
      this.accads = allAds.json();
      console.log("add acc", this.accads);
    })
  }

  adsaccDetail(adsId, postadd, img) {
    console.log("addid", adsId)
    console.log("postad", postadd)
    let modal = this.modalCtrl.create(ModalVerSharePage, {
      adsId : adsId,
      postaddid: postadd,
      imagepath: img,
      token : this.token,
      userId : this.userId
    //console.log(this.adsacc);
    });
    modal.present();
  }

  getAllPointReq(userMark) {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    this.http.get(this.urlPublic.apiUrl + 'api/Marketer/Verify/List', {headers: header}).subscribe((pend) => {
      
      let userMarketerPoint = pend.json();
      let all = [];
      userMarketerPoint.forEach(function (row, i) {
        let specificMarketer = userMark.find(x => x.userId == row.userId);
        console.log("this spec", specificMarketer)
        all.push({
          userPoinId: row.userPoinId,
          userName: specificMarketer ? specificMarketer.name : '',
          email: specificMarketer ?  specificMarketer.email : '',
          userId: row.userId,
          pointNominal: row.pointNominal,
          status: row.status,
          transactionDate: row.transactionDate,
          nominal: row.nominal,
          poinType: row.poinType,
          poinTypeId: row.poinTypeId,
          userPoinRelease: row.userPoinRelease,
          userPointImage: row.userPointImage
        })
      });
      // let specificMarketer = userMarketer.filter(x => x.userId == this.userId)
      // this.pointsReq = specificMarketer
      // this.pathImg = this.points.userPointImage.imageUrl
      this.marketers = all;
      console.log("get all points req",userMarketerPoint);
      console.log("get all user marketer",userMark);
      console.log("get all ",this.marketers);

      
    })
  }


  getUserVendor() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Users/Vendor', {headers: header}).subscribe(vendorData => {
      let thisvendors = vendorData.json();
    })
  }

  getUserMarketer() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Users/Marketer', {headers: header}).subscribe(marketData => {
      let userMark = marketData.json();
      this.getAllPointReq(userMark);
    })
  }

  getPoin(userVendorId) {
    let modal = this.modalCtrl.create(ModalVerPage, {
      userId : userVendorId,
      token : this.token,
      typeModal: 'Vendor'
    });
    modal.present();
  }

  getRequestPoin(userMarketId) {
    let modal = this.modalCtrl.create(ModalVerPage, {
      userId : userMarketId,
      token : this.token,
      typeModal : 'Marketer'
    });
    modal.present();
  }

  getShare(userMarketId) {
    let modal = this.modalCtrl.create(ModalVerSharePage, {
      userId : userMarketId,
      token : this.token,
      typeModal : 'Marketer'
    });
    modal.present();
  }

  


  logOut() {
    window.localStorage.removeItem('username');
    window.localStorage.removeItem('password');
    this.presentLoading('');
    window.location.reload();
  }

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

  // alert

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // alert
  

  

}
