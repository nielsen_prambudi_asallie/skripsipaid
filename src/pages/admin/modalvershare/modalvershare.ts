import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, ViewController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { UrlPublic } from '../../urlpublic';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-modalvershare',
  templateUrl: 'modalvershare.html'
})
export class ModalVerSharePage {

  token: any;
  userId: any;
  adsId: any;
  title: any;
  description: any;
  bigImg = null;
  marketerTypeId: any;
  marketerType: any;
  userAdsImg: any;
  adsToFb: any;
  promoted: boolean;
  file: any;
  postadd: any;
  imgver = null;
  imgurl: any;
  folderName: any = 'evidence'; 

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public socialSharing: SocialSharing,
    public viewCtrl: ViewController,
    public urlPublic: UrlPublic,
    public navParams: NavParams,
    public sanitizer: DomSanitizer,
    public loadingCtrl: LoadingController,
    public http: Http
    ) {
      this.token = navParams.get('token');
      this.userId = navParams.get('userId');
      this.adsId = navParams.get('adsId');
      this.postadd = navParams.get('postaddid');
      this.imgurl = navParams.get('imagepath');
      this.getImageVerify(this.imgurl);
      console.log("postadd", this.postadd)
      this.getDetailAds(this.adsId)
  }

// get ads detil
  getDetailAds(adsId) {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Adds/Marketer/' + adsId, {headers: header}).subscribe((ads) => {
      let allAds = ads.json();
      console.log(allAds)
      this.promoted = allAds.promoted
      this.title = allAds.ads.title;
      this.bigImg = allAds.ads.imgUrl;
      this.description = allAds.ads.description;
      this.marketerTypeId = allAds.ads.cityId;
      this.http.get(this.urlPublic.apiUrl + 'api/Users/City/' + this.marketerTypeId).subscribe(city => {
        let findcity = city.json();
        this.marketerType = findcity.nama;
      })
      var header1 = new Headers();
      header1.append('Authorization', 'bearer' + ' ' + this.token)
      this.http.get(this.urlPublic.apiUrl + 'api/Image?imagePath=' + this.bigImg, {headers: header1}).subscribe(imgData => {
        let url = imgData.url
        this.userAdsImg = url.replace("'\'", "'\\'")
        console.log("url detil", this.userAdsImg)
        this.getImgContent();
      })
    })
  }

  getImageVerify(img) {
    var header1 = new Headers();
      header1.append('Authorization', 'bearer' + ' ' + this.token)
      this.http.get(this.urlPublic.apiUrl + 'api/Image?imagePath=' + img, {headers: header1}).subscribe(imgData => {
        let url = imgData.url
        this.imgver = url.replace("'\'", "'\\'")
        console.log("url detil", this.imgver)
        this.getImgVerify();
    })
  }


  getImgVerify(): SafeUrl {  
    return this.sanitizer.bypassSecurityTrustUrl(this.imgver);
  }
  getImgContent(): SafeUrl {  
    return this.sanitizer.bypassSecurityTrustUrl(this.userAdsImg);
  }

  changeListener(event) {
    
    this.file = event.target.files[0];
    console.log("get pic", this.file)
  }

  uploadFile() {
    let add = {
      postAddid: this.postadd
    }
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.post(this.urlPublic.apiUrl + 'api/Adds/Marketer/verify', add ,{headers: header}).subscribe(
        share => {
            let shares = share.json()
            this.presentPrompt('Berhasil', 'Verify berhasil')
        }
    )
  }


  

  // get ads detil

  // share to facebook
  
  share() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Adds/Vendor/' + this.adsId, {headers: header}).subscribe((ads) => {
      let allAds = ads.json();
      console.log(allAds)
      this.title = allAds.title;
      this.bigImg = allAds.imgUrl;
      this.description = allAds.description;
      this.marketerTypeId = allAds.cityId;
      this.http.get(this.urlPublic.apiUrl + 'api/Users/City/' + this.marketerTypeId).subscribe(city => {
        let findcity = city.json();
        this.marketerType = findcity.nama;
      })
      var header1 = new Headers();
      header1.append('Authorization', 'bearer' + ' ' + this.token)
      this.http.get(this.urlPublic.apiUrl + 'api/Image?imagePath=' + this.bigImg, {headers: header1}).subscribe(imgData => {
        let url = imgData.url
        this.adsToFb = url.replace("'\'", "'\\'")
        console.log("url in share to FB", this.adsToFb)
        this.getImgContent();
        this.toFB()
      })
    })
    
    
  }

  toFB() {
    this.socialSharing.shareViaFacebook(this.title, null, this.adsToFb)
    .then(() => {
        this.presentLoading("")
        let idShare = {
            userId: this.userId,
            adsId: this.adsId,
            link: ""
        }
        var header = new Headers();
        header.append('Authorization', 'bearer' + ' ' + this.token)
        this.http.post(this.urlPublic.apiUrl + 'api/Adds/Marketer', idShare ,{headers: header}).subscribe(
            share => {
                let shares = share.json()
                // this.presentPrompt('Berhasil', 'Bagikan lewat Facebook berhasil')
            }
        )
    })
    .catch(() => {

    })
    this.presentPrompt('Berhasil', 'Bagikan lewat Facebook berhasil')
  }

  // share to facebook

  dismiss() {
      this.viewCtrl.dismiss()
  }


  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

  // Alert

  presentPrompt(titlePrompt, messagePrompt) {
    let nextStep = this.alertCtrl.create({
      title: titlePrompt,
      message: messagePrompt,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            window.location.reload()
          }
        }
      ]
    })
    nextStep.present()
  }

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // Alert
  

  
  

  

}
