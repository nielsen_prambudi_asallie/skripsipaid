import { UrlPublic } from './../urlpublic';
import { Component } from '@angular/core';
import { ModalController, NavController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'page-admin-report',
  templateUrl: 'adminreport.html'
})
export class AdminReportPage {

  token: any;
  userId: any;
  vendors: any;
  vendorsLength: any;
  marketers: any;
  marketersLength: any; 

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public urlPublic: UrlPublic,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public http: Http,
    public sanitizer: DomSanitizer
    ) {
      this.token = navParams.data.token;
      this.userId = navParams.data.userId;
      console.log(this.token)
      this.getUserVendor()
      this.getUserMarketer()
  }

  doRefresher(refresher){
    this.getUserVendor();
    this.getUserMarketer();
    //this.getAllPoint();
    //console.log(this.getAllPoint);
    //console.log(this.getAllPointReq)

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  getUserVendor() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Users/Vendor', {headers: header}).subscribe(vendorData => {
      this.vendors = vendorData.json();
      this.vendorsLength = this.vendors.length;
      console.log("length vendor", this.vendors.length )
    })
  }

  getUserMarketer() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Users/Marketer', {headers: header}).subscribe(marketData => {
      this.marketers = marketData.json();
      this.marketersLength = this.marketers.length;
    })
  }

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

  // alert

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // alert
  

  

}
