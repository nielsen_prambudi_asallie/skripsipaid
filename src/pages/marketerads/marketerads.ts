import { UrlPublic } from './../urlpublic';
import { ModalAdsPage } from './modalads/modalads';
import { ModalAdsAccPage } from './modaladsacc/modaladsacc';
import { Component } from '@angular/core';
import { ModalController, NavController, LoadingController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import moment from 'moment';

@Component({
  selector: 'page-marketerads',
  templateUrl: 'marketerads.html'
})
export class MarketerAdsPage {

  token: any;
  userId: any;
  adsId: any;
  adsacc: any;
  ads: any;
  accads: any;
  title: any;
  userAdsImg: any;
  userAccImg: any;
  description: any;
  imgUrl: any;
  startDate: any;
  endDate: any;  
  startDateAcc: any;
  endDateAcc: any; 

  constructor(public navCtrl: NavController,
    public urlPublic: UrlPublic,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public http: Http,
    public sanitizer: DomSanitizer,
    
    ) {
      this.token = navParams.data.token;
      this.userId = navParams.data.userId;
      console.log(this.token);
      this.endDate = moment().format('YYYY-MM-DD');
      this.startDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
      this.startDateAcc = moment().format('YYYY-MM-DD');
      this.endDateAcc = moment().subtract(15, 'days').format('YYYY-MM-DD');
      this.getMarketerAds();
      this.getAcceptedAds();
  }

  // get marketer ads

  getMarketerAds() {
    console.log(this.endDate)
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Adds/Marketer?startDate=' + this.startDate + '&endDate=' + this.endDate, {headers: header}).subscribe(allAds => {
      this.ads = allAds.json();
      console.log(this.ads);
    })
  }

  

  getAcceptedAds() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Adds/Marketer/Adds?startDate=' + this.startDateAcc + '&endDate=' + this.endDateAcc, {headers: header}).subscribe(allAds => {
      this.accads = allAds.json();
      console.log("add acc", this.accads);
    })
  }

  // get marketer ads

  adsDetail(adsId) {
    let modal = this.modalCtrl.create(ModalAdsPage, {
      adsId : adsId,
      token : this.token,
      userId : this.userId
    });
    modal.present();
  }

  adsaccDetail(adsId, postadd) {
    console.log("addid", adsId)
    console.log("postad", postadd)
    let modal = this.modalCtrl.create(ModalAdsAccPage, {
      adsId : adsId,
      postaddid: postadd,
      token : this.token,
      userId : this.userId
    //console.log(this.adsacc);
    });
    modal.present();
  }
}
