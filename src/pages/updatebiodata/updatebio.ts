import { UrlPublic } from './../urlpublic';
import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { HomePage } from './../home/home';

@Component({
  selector: 'page-updatebio',
  templateUrl: 'updatebio.html'
})
export class UpdateBiodataPage {

  userId: any;
  token: any;
  updateUser: any;
  updateError: any;
  name      : any;
  email     : any;
  address   : any;
  city      : any;
  cityId    : any;
  phone     : any;
  regType   : any;
  regTypeId : any;
  username  : any;
  password  : any;

  

  constructor(public navCtrl: NavController,
    public urlPublic: UrlPublic,
    public http: Http,
    public navParams: NavParams, 
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    ) {
        this.userId = navParams.get('userId')
        this.token = navParams.get('token')
        this.getBiodata()
  }

//   get first biodata to see the value

  getBiodata() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    this.http.get(this.urlPublic.apiUrl + 'api/Users/' + this.userId, {headers: header}).subscribe(data => {
      let userBio = data.json();
      this.name = userBio.name;
      this.email = userBio.email;
      this.address = userBio.address;
      this.phone = userBio.phone;
      this.username = userBio.userName;
      this.password = userBio.password;
      this.cityId = userBio.cityId;
      this.regTypeId = userBio.regType;
      console.log("phone", this.phone)
      this.http.get(this.urlPublic.apiUrl + 'api/Users/City/' + this.cityId).subscribe(city => {
        let findcity = city.json();
        this.city = findcity.nama;
      })
      this.http.get(this.urlPublic.apiUrl + 'api/Users/RegisterType').subscribe((reg) => {
        let allReg = reg.json();
        let findreg = allReg.find(x => x.regTypeId == this.regTypeId);
        this.regType = findreg.regType1;
      })
    })

    

    
  }

//   get first biodata to see the value


// update biodata

  updateBio() {
    this.presentLoading("")
    let updateVar = JSON.stringify({
      userId: this.userId,
      name: this.name,
      email: this.email,
      address: this.address,
      phone: this.phone,
      userName: this.username,
      password: this.password,
      cityId: this.cityId,
      regType: this.regTypeId
    })
    console.log("variable update", updateVar)
    var header = new Headers();
    header.append('Content-Type', 'application/json');
    header.append('Access-Control-Allow-Origin', '*');
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.put(this.urlPublic.apiUrl + 'api/Users/register', updateVar, {headers: header}).subscribe(
      reg => {
        let reguser = reg.json()
        this.presentPrompt('Konfirmasi Berhasil', 'Data anda telah diperbaharui' )
      }, err => {
        this.presentLoading("failed")
      }
    )
    
  }

// update biodata

  presentPrompt(titlePrompt, messagePrompt) {
    let nextStep = this.alertCtrl.create({
      title: titlePrompt,
      message: messagePrompt,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            if(titlePrompt == "Konfirmasi Berhasil"){
              window.location.reload()
            }
          }
        }
      ]
    })
    nextStep.present()
  }

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

  // Alert

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // Alert
  

}
