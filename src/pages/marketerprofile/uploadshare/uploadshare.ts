import { UrlPublic } from './../../urlpublic';
import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { AlertController, NavController, LoadingController, NavParams } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Http, Headers, ResponseType } from '@angular/http';


@Component({
    selector: 'page-uploadshare',
    templateUrl: 'uploadshare.html'
})

export class UploadSharePage {

    token: any;
    userId: any;
    poinId: any;
    submitted: boolean;
    image: any;
    public myPhoto: any;
    bigImg = null;
    bigSize = '0';
    file: any;
    folderName: any = 'transfer'; 

    constructor(public navCtrl: NavController,
        public urlPublic: UrlPublic,
        public sanitizer: DomSanitizer,
        public alertCtrl: AlertController,
        private transfer: FileTransfer,
        public http: Http,
        private camera: Camera,
        public loadingCtrl: LoadingController,
        public navParams: NavParams) {
            this.token = navParams.get('token');
            this.userId = navParams.get('userId');
            this.poinId = navParams.get('poin');
    }
  
  // upload image


  // getImage(): void {
  
  //   this.camera.getPicture({
  //     quality: 100,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
  //   }).then(imageData => {

  //     // get the data
  //     this.myPhoto = imageData;
  //     // get the data

  //     // converting as png format
  //     let base64Data = 'data:image/jpeg;base64,' + imageData;
  //     this.bigImg = base64Data;
  //     this.bigSize = this.getImageSize(this.bigImg)
  //     // converting as png format

  //   }, (err) => {
  //     this.presentPrompt("Gagal","File tidak dapat di unggah hubungi administrator");
  //   });
  // }

  // // sanitize url so it can read as link url
  // getImgContent(): SafeUrl {
  //   return this.sanitizer.bypassSecurityTrustUrl(this.myPhoto)
  // }
  // // sanitize url so it can read as link url

  // getImageSize(data_url) {
  //   // get size of image
  //   var head = 'data:image/jpeg;base64,';
  //   return ((data_url.length - head.length) * 3 / 4 / (1024*1024)).toFixed(4);
  //   // get size of image
  // }

  // uploadFile() {
  //   console.log(this.bigImg)
  //   let loader = this.loadingCtrl.create({
  //     content: "Uploading..."
  //   });

    
  //   loader.present();
  //   const fileTransfer: FileTransferObject = this.transfer.create();
  //   var header = new Headers();
  //   header.append('Authorization', 'bearer' + ' ' + this.token)
  //   header.append('Content-Type', 'application/json');

  //   let itbe = {
  //     file: this.myPhoto
  //   }
  //   this.http.post(this.urlPublic.apiUrl + 'api/Image?folderPath=' + this.folderName, itbe, {headers: header}).subscribe(
  //     data => {
  //       console.log(data.json())
  //     }
  //   )
  
  //   let options: FileUploadOptions = {
  //     fileKey: "file",
  //     fileName: "beli" + this.poinId + ".jpg",
  //     chunkedMode: false,
  //     httpMethod: "POST",
  //     mimeType: "image/jpeg",
  //     headers: header
  //   }
  
  //   fileTransfer.upload(this.bigImg, this.urlPublic.apiUrl + 'api/Image?folderPath=' + this.folderName, options)
  //     .then(data => {
  //     console.log(data.response+" Uploaded Successfully");
  //     loader.dismiss();
  //     this.presentPrompt("Konfirmasi Berhasil", "Gambar telah berhasil di unggah");
  //   }, (err) => {
  //     console.log(err);
  //     loader.dismiss();
  //     this.presentPrompt("Gagal", "File tidak dapat di unggah hubungi administrator");
  //   });
  // }

  changeListener(event) {
    
    this.file = event.target.files[0];
    console.log("get pic", this.file)
  }

  uploadFile() {
    var fd = new FormData();
    fd.append('file', this.file, this.file.name)
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token);
    header.append('ResponseType', 'text' as 'text')

    this.http.post(this.urlPublic.apiUrl + 'api/Image?folderPath=' + this.folderName, fd, {headers: header}).subscribe(
      data => {
        
        let dataImage = data.text();
        console.log("image transfer", dataImage)
        let image = {
          userPointId: this.poinId,
          imageUrl: dataImage
        }
        var header = new Headers();
        header.append('Authorization', 'bearer' + ' ' + this.token);
        header.append('Content-Type', 'application/json');
        this.http.post(this.urlPublic.apiUrl + 'api/Poin/Purchase/Image', image, {headers: header}).subscribe(
          data => {
            let dataPurchase = data.json();
            this.presentPrompt("Konfirmasi Berhasil", "Gambar telah berhasil di unggah");

          }, (err) => {
            this.presentLoading("failed")
          }
        )
      }
    )
  }

  // upload image


  // alert

  presentPrompt(titlePrompt, messagePrompt) {
    let nextStep = this.alertCtrl.create({
      title: titlePrompt,
      message: messagePrompt,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            if(titlePrompt == "Konfirmasi Berhasil"){
              window.location.reload()
            }
          }
        }
      ]
    })
    nextStep.present()
  }

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // alert

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading
}