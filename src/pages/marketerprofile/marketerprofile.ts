import { UrlPublic } from './../urlpublic';
import { MarketerAdsPage } from './../marketerads/marketerads';
import { UploadSharePage } from './uploadshare/uploadshare';
import { UpdateBiodataPage } from './../updatebiodata/updatebio';
import { Component } from '@angular/core';
import { AlertController, NavController, LoadingController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';

// ctrl+F untuk mencari function di bawah ini :

// get user profile
// get marketer ads
// get user point
// update Biodata
// add works
// log out
// alert
// loading


@Component({
  selector: 'page-marketerprofile',
  templateUrl: 'marketerprofile.html'
})
export class MarketerProfilePage {

  token: any;
  userId: any;
  cityId: any;
  ads: any;
  name: string;
  address: string;
  city: string;
  phone: any;
  pointAmount: any;
  totalPoin: any;
  totalMoney: any;
  startDate: any;
  endDate: any;

  constructor(public navCtrl: NavController,
    public urlPublic: UrlPublic,
    public alertCtrl: AlertController,
    public http: Http,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,) {
    this.token = navParams.data.token;
    this.userId = navParams.data.userId;
    this.pointAmount = 0;
    this.totalPoin = 0;
    this.totalMoney = 0;
    this.getUserProfile();
    this.getUserPoint();
    this.totalAllPoint();
  }

  
  // get user profile

  getUserProfile() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Users/' + this.userId, {headers: header}).subscribe(data => {
      let userBio = data.json();
      this.name = userBio.name;
      this.address = userBio.address;
      this.phone = userBio.phone;
      this.cityId = userBio.cityId;
      console.log(this.cityId)
      this.http.get(this.urlPublic.apiUrl + 'api/Users/City/' + this.cityId).subscribe(city => {
        let allCity = city.json();
        this.city = allCity.nama;
      })
    })
    
    
  }

  // get user profile


  // get marketer ads

  getMarketerAds() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Adds/Marketer/Adds?startDate=' + this.startDate + '&endDate=' + this.endDate, {headers: header}).subscribe(allAds => {
      this.ads = allAds.json();
    })
  }

  // get marketer ads


  // get user point

  getUserPoint() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlPublic.apiUrl + 'api/Poin/Sum/' + this.userId, {headers: header}).subscribe(pointData => {
      let userPoint = pointData.json();
      this.pointAmount = userPoint.pointNominal;
      
    })
    
    
  }

  // get user point

  totalAllPoint() {
    this.totalMoney = this.totalPoin * 1000
  }

  agree() {
    let pointDemand = JSON.stringify ({
      userId: this.userId,
      pointNominal: this.totalPoin
    })
    var header = new Headers();
    header.append('Content-Type', 'application/json');
    header.append('Access-Control-Allow-Origin', '*');
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.post(this.urlPublic.apiUrl + 'api/Marketer/Release', pointDemand, {headers: header}).subscribe(
      demand => {
        let poindemand = demand.json()
        this.presentPrompt('Konfirmasi Berhasil', 'Poin telah diajukan, harap menunggu verifikasi dari administrator' )
      }, err => {
        this.presentLoading("failed")
      }
    )

  }

  // update biodata
  updateBio() {
    this.navCtrl.push(UpdateBiodataPage, {userId : this.userId, token : this.token})
  }

  // update biodata
  
  // add work
  backToAds() {
    this.navCtrl.push(MarketerAdsPage, {userId : this.userId, token : this.token})
  }

  // add work

  goToUpload() {
    this.navCtrl.push(UploadSharePage);
  }


  // log out
  logOut() {
    window.localStorage.removeItem('username');
    window.localStorage.removeItem('password');
    this.presentLoading('');
    window.location.reload();
  }
  // log out

  // alert

  presentPrompt(titlePrompt, messagePrompt) {
    let nextStep = this.alertCtrl.create({
      title: titlePrompt,
      message: messagePrompt,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            if(titlePrompt == "Konfirmasi Berhasil"){
              window.location.reload()
            }
          }
        }
      ]
    })
    nextStep.present()
  }

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // alert

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading



}
