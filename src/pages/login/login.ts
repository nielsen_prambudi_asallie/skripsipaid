import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { UrlPublic } from '../urlpublic';
import { Http, Headers } from '@angular/http';;


// ctrl+F untuk mencari function di bawah ini :

// To homepage
  // login
  // sign in after getting local storage
  // Login with FB
// to register
// loading
// Alert
    

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  username: '';
  password: '';
  regType: any;
  city: any;
  cities : any;
  regs : any;

  shareObj = {
    href: "FACEBOOK-SHARE-LINK",
    hashtag:"#FACEBOOK-SHARE-HASGTAG"
  };


  constructor(public navCtrl: NavController,
     public alertCtrl: AlertController,
     public loadingCtrl: LoadingController,
     public urlPublic: UrlPublic, 
     public http: Http,
     public navParams: NavParams,
     public fb: Facebook,
     public storage: Storage
     ) {
       let username = navParams.get('username');
       let password = navParams.get('password');
       console.log(username)
       if (this.username != '' && this.password != '') {
        console.log(username)
         this.signinGo(username, password)
       }
       
  }

   

  // to homepage

  // login

  signin() {
      window.localStorage.setItem('username', this.username)
      window.localStorage.setItem('password', this.password) 
      var data = JSON.stringify({"userName": this.username, "password": this.password})
      var header = new Headers();
      header.append('Content-Type', 'application/json');
      this.presentLoading("")
      this.http.post(this.urlPublic.apiUrl + 'api/Users/authenticate', data, {headers: header}).subscribe(
        data => {
          let dataUser = data.json()
          this.navCtrl.setRoot(HomePage, {
            type: dataUser.regType,
            token: dataUser.token,
            userId: dataUser.userId,
          })
        }, err => {
          this.presentLoading("failed")
          
        }
      )
  }
  // login

  // sign in after getting local storage
signinGo(username, password) {
    var data = JSON.stringify({"userName": username, "password": password})
    var header = new Headers();
    header.append('Content-Type', 'application/json');
    this.presentLoading("")
    this.http.post(this.urlPublic.apiUrl + 'api/Users/authenticate', data, {headers: header}).subscribe(
      data => {
        let dataUser = data.json()
        this.navCtrl.setRoot(HomePage, {
          type: dataUser.regType,
          token: dataUser.token,
          userId: dataUser.userId,
        })
      }, err => {
        // this.presentLoading("failed")
      }
    )
}
// sign in after getting local storage



// Login with FB
loginFB(){
  // Login with permissions
  this.fb.login(['public_profile', 'user_photos', 'email', 'user_birthday'])
  .then( (res: FacebookLoginResponse) => {

      console.log("get res on facebook", res)
      // The connection was successful
      if(res.status == "connected") {

          // Get user ID and Token
          var fb_id = res.authResponse.userID;
          var fb_token = res.authResponse.accessToken;

          // Get user infos from the API
          this.fb.api("/me?fields=name,gender,birthday,email", []).then((user) => {

              // Get the connected user details
              var name      = user.name;
              var email     = user.email;

              console.log("=== USER INFOS ===");
              console.log("user info", user)
              console.log("Name : " + name);
              console.log("Email : " + email);

              var data = JSON.stringify({"userName": name, "password": "loginf13"})
              var header = new Headers();
              header.append('Content-Type', 'application/json');
              header.append('Access-Control-Allow-Origin', '*');
              this.presentLoading("")
              this.http.post(this.urlPublic.apiUrl + 'api/Users/authenticate', data, {headers: header}).subscribe(
                data => {
                  let dataUser = data.json()
                  this.navCtrl.setRoot(HomePage, {
                    type: dataUser.regType,
                    token: dataUser.token,
                    userId: dataUser.userId,
                  })
                }, err => {
                  this.navCtrl.push(RegisterPage, {
                    username: name,
                    email: email,
                    password: "loginf13"
                  })
                }
              )

              

              // => Open user session and redirect to the next page

              


          });

      } 
      // An error occurred while loging-in
      else {

          console.log("An error occurred...");

      }

  })
  .catch((e) => {
      console.log('Error logging into Facebook', e);
  });
}
// Login with FB

  // to homepage


  // to register

  register() {
      this.navCtrl.push(RegisterPage)
  }

  // to register

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

  // Alert

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // Alert

}
