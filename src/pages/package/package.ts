import { UrlPublic } from './../urlpublic';
import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import moment from 'moment';

@Component({
  selector: 'page-package',
  templateUrl: 'package.html'
})
export class PackagePage {

  userId: any;
  token: any;
  package: any;

  constructor(public navCtrl: NavController,
     public http: Http,
     public urlpublic: UrlPublic,
     public alertCtrl: AlertController,
     public navParams: NavParams,
     public loadingCtrl: LoadingController, ) {
      this.userId = navParams.get('userId')
      this.token = navParams.get('token')
      this.getPackage()
  }
  

  // get package
  getPackage() {
    var header = new Headers();
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.get(this.urlpublic.apiUrl + 'api/Poin/Purchase/Type', {headers: header}).subscribe((pack) => {
      this.package = pack.json()
      console.log("all package", this.package)
    })
  }
  // get package

  // submit package

  submitPackage(onePack) {
    let subPackage = {
      userId : this.userId,
      pointNominal : onePack.paketPoint,
      poinType: {
        paketId: onePack.paketId,
        poinType1: "add"
      }
    }
    console.log(subPackage)
    this.presentLoading("")
    var header = new Headers();
    header.append('Content-Type', 'application/json');
    header.append('Authorization', 'bearer' + ' ' + this.token)
    this.http.post(this.urlpublic.apiUrl + 'api/Poin/Purchase', subPackage, {headers: header}).subscribe(
      packReg => {
        let subPack = packReg.json();
        console.log("success pack", subPack)
        this.presentPrompt("Konfirmasi Berhasil", "Poin anda telah terdaftar silahkan unggah bukti transfer anda")

      }, err => {
        this.presentLoading("failed")
      }
    )
  }

  // submit package

  // loading

  presentLoading(status) {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      if(status == "failed") {
        this.errorAlert();
      } 
      
    }, 1000);
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  // loading

  // Alert


  presentPrompt(titlePrompt, messagePrompt) {
    let nextStep = this.alertCtrl.create({
      title: titlePrompt,
      message: messagePrompt,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            window.location.reload()
          }
        }
      ]
    })
    nextStep.present()
  }

  errorAlert() {
    let error = this.alertCtrl.create({
      title: "Error Message",
      message: "Terjadi kesalahan terhadap sistem, silahkan hubungi administrator atau cek kembali jaringan internet anda",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok')
          }
        }
      ]
    })
    error.present();
  }

  // Alert

}
