import {AutoCompleteService} from 'ionic2-auto-complete';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map'
import { Http, Headers } from '@angular/http';
import { PublicFeature } from '@angular/core/src/render3';
import { UrlPublic } from './urlpublic';


// put injectable outside @component 

@Injectable()
export class CompleteTestService implements AutoCompleteService {
  labelAttribute = "nama";
  formValueAttribute = "cityId";

  constructor(private http:Http, 
    public urlPublic : UrlPublic) {
  
  }

  getResults(keyword:string) {
    return this.http.get(this.urlPublic.apiUrl + 'api/Users/City?search='+keyword)
      .map(
        result =>
        {
          console.log(result.json());
          return result.json();
            //.filter(item => item.nama.toLowerCase().startsWith(keyword.toLowerCase()) )
        });
  }
}
