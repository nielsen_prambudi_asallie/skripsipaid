import { Component, ViewChild, NgModule } from '@angular/core';

import { Platform, Nav, IonicModule} from 'ionic-angular';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { AutoCompleteModule } from 'ionic2-auto-complete';
import { RegisterPage } from '../pages/register/register';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


@Component({
  templateUrl: 'app.html'
})

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RegisterPage,

  ],
  imports: [
    AutoCompleteModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      let username = window.localStorage.getItem('username') ? window.localStorage.getItem('username') : '';
      let password = window.localStorage.getItem('password') ? window.localStorage.getItem('password') : '';
      if (username != undefined &&  password != undefined) {
        this.nav.setRoot(LoginPage, {
          username: username,
          password: password
        })
      } else {
        this.rootPage = LoginPage;
      }
    });
  }
}
