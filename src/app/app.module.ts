import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler, Modal } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';


import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { AdsPage } from '../pages/ads/ads';
import { PackagePage } from '../pages/package/package';
import { VendorProfilePage } from '../pages/vendorprofile/vendorprofile';
import { UpdateBiodataPage } from './../pages/updatebiodata/updatebio';
import { MarketerProfilePage } from './../pages/marketerprofile/marketerprofile';
import { ModalAdsAccPage } from './../pages/marketerads/modaladsacc/modaladsacc';
import { MarketerAdsPage } from './../pages/marketerads/marketerads';
import { ModalAdsPage } from './../pages/marketerads/modalads/modalads';
import { UploadTransferPage } from './../pages/vendorprofile/uploadtrf/uploadtrf';
import { ModalVerPage } from './../pages/admin/modalver/modalver';
import { ModalVerSharePage } from './../pages/admin/modalvershare/modalvershare';
import { ModalImgPage } from './../pages/admin/modalimg/modalimg';
import { AdminPage } from './../pages/admin/admin';
import { UrlPublic } from './../pages/urlpublic';
import { CompleteTestService } from './../pages/auto';
import { AdminReportPage } from '../pages/adminreport/adminreport';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Facebook } from '@ionic-native/facebook';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { SocialSharing } from '@ionic-native/social-sharing';

import { AutoCompleteModule } from 'ionic2-auto-complete';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    AdsPage,
    PackagePage,
    VendorProfilePage,
    UploadTransferPage,
    UpdateBiodataPage,
    MarketerAdsPage,
    MarketerProfilePage,
    ModalAdsAccPage,
    AdminPage,
    AdminReportPage,
    ModalAdsPage,
    ModalVerPage,
    ModalVerSharePage,
    ModalImgPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AutoCompleteModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    AdsPage,
    PackagePage,
    VendorProfilePage,
    UploadTransferPage,
    UpdateBiodataPage,
    MarketerAdsPage,
    MarketerProfilePage,
    ModalAdsAccPage,
    AdminPage,
    AdminReportPage,
    ModalAdsPage,
    ModalVerPage,
    ModalVerSharePage,
    ModalImgPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CompleteTestService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Facebook,
    FileTransfer,
    FileTransferObject,
    File,
    Camera,
    SocialSharing,
    UrlPublic
  ]
})
export class AppModule {}
